<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

    <body>
    <div style="width:424px">
    <?php
    $n=4;
    $m=$n*$n;
    $line=1;
    for($i=1;$i<=$m;$i++)
    {
        if ($i % 2 == 0)
            $color = "black";
        else
            $color = "gray";
        if ($line % 2 == 0) {
            $float = "right";
        } else {
            $float = "left";
        }
        ?>
        <div id="box" style="background-color:<?php echo $color ?>; float:<?php echo $float ?>">
            <?php
            echo $i;
            ?>
        </div>
        <?php
        if ($i % $n == 0) {
            $line++;
        }
    }
    ?>
    </div>
    </body>


</html>