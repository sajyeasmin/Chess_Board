<html>
<title>Matrix</title>
<head></head>
<body>
<form method="get">
    <input name="matrix" type ="text" placeholder="Matrix"></input>
    <input type ="submit" value="Go">

    <?php
    if(isset($_GET["matrix"])) {
    $n = $_GET["matrix"];
    $m= $n*$n;
    $width = 100* $n+6*$n;
    ?>
    <div style="width:<?php echo $width; ?>">
        <br>
        <?php
        $line=1;
        for ($i = 1; $i <= $m; $i++) {
            if($i%2==0){
                $color="black";
            }
            else{
                $color="gray";
            }
            if($line%2==0)
            {
                $float="right";
            }
            else
            {
                $float="left";
            }
            ?>
            <div
                style="width:100px;height:100px;float:<?php echo $float;?>;
                    border-style: solid;color:white;background-color:<?php echo $color;?>;
                    font: 125%;text-align: center;line-height: 100px;margin:0px;">
                <?php echo $i; ?>
            </div>
            <?php
            if($i%$n == 0)
            {
                $line++;
            }
        }
        }
        ?>
    </div>
</form>
</body>
</html>